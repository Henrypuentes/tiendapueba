package com.mintic.s5.hackers.parking.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class PersonaDAO {
	private ConnectionManager con;
	private Connection connection;

	public PersonaDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {
		System.out.println(jdbcURL);
		con = new ConnectionManager(jdbcURL, jdbcUsername, jdbcPassword);
	}
	
	
	// insertar persona
	public boolean insertar(Persona persona) throws SQLException {
		String sql = "INSERT INTO persona (  nombre, fecha_nacimiento, id_tipo_documeto , direccion, numero_doc) VALUES (?, ?, ?,?,?)";
			con.conectar();
			connection = con.getJdbcConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(5, persona.getNumero_doc());
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getFecha());
			statement.setInt(3, persona.getTipo_documento());
			statement.setString(4, persona.getDireccion());
			

			boolean rowInserted = statement.executeUpdate() > 0;
			statement.close();
			con.desconectar();
			return rowInserted;
		}

}
