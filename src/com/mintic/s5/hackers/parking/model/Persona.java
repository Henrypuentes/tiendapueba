package com.mintic.s5.hackers.parking.model;

public class Persona {

	private int id;
	private String nombre;
	private String fecha;
	private int tipo_documento;
	private String direccion;
	private String numero_doc;
	private String pruebaNuevaLinea;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(int tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNumero_doc() {
		return numero_doc;
	}
	public void setNumero_doc(String numero_doc) {
		this.numero_doc = numero_doc;
	}
	public int getId() {
		return id;
	}
	
	
}
