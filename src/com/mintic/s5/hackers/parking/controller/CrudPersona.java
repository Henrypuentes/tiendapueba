package com.mintic.s5.hackers.parking.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mintic.s5.hackers.parking.model.PersonaDAO;

/**
 * Servlet implementation class CrudPersona
 */
@WebServlet("/CrudPersona")
public class CrudPersona extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	private PersonaDAO personaDao;

    /**
     * Default constructor. 
     */
    public CrudPersona() {
    	String jdbcURL = getServletContext().getInitParameter("jdbcURL");
    	String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
		String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");
		
		try {
			personaDao = new PersonaDAO(jdbcURL, jdbcUsername, jdbcPassword);
		} catch (SQLException e) {
			System.out.println("hubo un problema al conectarme :"+ e.getMessage());
		}

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
