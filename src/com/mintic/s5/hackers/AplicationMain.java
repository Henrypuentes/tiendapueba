package com.mintic.s5.hackers;
// Cambios de prueba
import java.sql.SQLException;

import com.mintic.s5.hackers.parking.model.Persona;
import com.mintic.s5.hackers.parking.model.PersonaDAO;

public class AplicationMain {
	
	public static void main(String[] args) {
		PersonaDAO personaDao;
		
    	String jdbcURL = "jdbc:mysql://localhost:3306/mintics5?useSSL=false";
    	String jdbcUsername = "root";
		String jdbcPassword = "root";
		
		Persona persona = new Persona();
		persona.setNombre("Pepito Perez 2");
		persona.setDireccion("calle falsa 1232142");
		persona.setTipo_documento(5);
		persona.setNumero_doc("213421412");
		persona.setFecha("1996-08-18");
		
		try {
			personaDao = new PersonaDAO(jdbcURL, jdbcUsername, jdbcPassword);
			if(personaDao.insertar(persona)) {
				System.out.println("Registgro insertado exitosamente");
			}else {
				System.out.println("no se pudo insertar el registro");
			}
		} catch (SQLException e) {
			System.out.println("hubo un problema al conectarme :"+ e.getMessage());
		}
		


	}

}
